﻿using Moq;
using System.Transactions;
using System.Data.Common;
using APICarPooling.Controllers;
using APICarPooling.Core.Model;
using APICarPooling.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using APICarPooling.Persistence.Data;
using APICarPooling.Logging.Interfaces;
using Microsoft.AspNetCore.Http;

namespace APICarPooling.Test;

public class CarsTest
{

    [Fact]
    public async Task AddAvailableCars_ReturnsOkResult_WhenCarsAddedSuccessfully()
    {
        // Arrange
        var mockCarPoolingService = new Mock<ICarPoolingService>();

        var controller = new CarPoolingController(
           mockCarPoolingService.Object);

        var cars = new List<CarAvailable>
            {
                new CarAvailable { Id = 1, Seats = 4 },
                new CarAvailable { Id = 2, Seats = 6 }
            };

        // Act
        var result = await controller.AddAvailableCars(cars);

        // Assert
        Assert.IsType<OkObjectResult>(result.Result);
        var okResult = result.Result as OkObjectResult;
        Assert.Equal(cars, okResult.Value);
    }

    [Fact]
    public async Task AddAvailableCars_ThrowsException_WhenCarsNotAvailable()
    {
        // Arrange

        var carsToLoadMock = new List<CarAvailable>();

        var mockCarPoolingService = new Mock<ICarPoolingService>();

        mockCarPoolingService.Setup(repo => repo.AddAvailableCarsService(carsToLoadMock)).Throws(new Exception("Test exception. Cars count == 0"));
        var controller = new CarPoolingController(
           mockCarPoolingService.Object);

        var cars = new List<CarAvailable>();
        // Act & Assert
        await Assert.ThrowsAsync<Exception>(() => controller.AddAvailableCars(cars));
    }

    [Fact]
    public async Task ReturnTheCarIsTravelling_ReturnsNotFoundResult_WhenGroupsAssignedToACar()
    {
        // Arrange
        var mockCarPoolingService = new Mock<ICarPoolingService>();

        var controller = new CarPoolingController(
           mockCarPoolingService.Object);

        int groupID = 1;
        var carsAssigned = new List<CarAvailable>
            {
                new CarAvailable { Id = 1, Seats = 4 },
                new CarAvailable { Id = 2, Seats = 6 }
            };

        // Act
        var result = await controller.ReturnTheCarOfTheGroup(groupID);


        // Assert
        Assert.IsType<NotFoundResult>(result.Result);
        var NotFoundResult = result.Result as NotFoundResult;
        Assert.Equal(404, NotFoundResult.StatusCode);
    }

    [Fact]
    public async Task ReturnTheCarIsTravelling_ThrowsException_WhenGroupsAreNotAvailable()
    {
        // Arrange

        var mockCarPoolingService = new Mock<ICarPoolingService>();

        int groupIDToMock = -1;

        mockCarPoolingService.Setup(repo => repo.ReturnTheCarOfTheGroupService(groupIDToMock)).Throws(new Exception("Test exception. the group ID is required and it must be greater than 0"));

        var controller = new CarPoolingController(
             mockCarPoolingService.Object);

        int groupID = -1;
        // Act & Assert
        await Assert.ThrowsAsync<Exception>(() => controller.ReturnTheCarOfTheGroup(groupID));
    }

}

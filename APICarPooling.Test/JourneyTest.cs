﻿using System;
using APICarPooling.Controllers;
using APICarPooling.Core.Interfaces;
using APICarPooling.Core.Model;
using APICarPooling.Logging.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace APICarPooling.Test
{
    public class JourneyTest
    {

        [Fact]
        public async Task DropOffGroupOfPeopleIfTraveledOrNot_ReturnsNotFoundResult_WhenGroupsAssignedToACar()
        {
            // Arrange

            var mockCarPoolingService = new Mock<ICarPoolingService>();

            var controller = new CarPoolingController(
               mockCarPoolingService.Object);

            int groupID = 1;

            // Act
            var result = await controller.RequestToBeDroppedOff(groupID);


            // Assert
            Assert.IsType<NotFoundResult>(result.Result);
            var NotFoundResult = result.Result as NotFoundResult;
            Assert.Equal(404, NotFoundResult.StatusCode);
        }

        [Fact]
        public async Task ReturnTheCarIsTravelling_ThrowsException_WhenGroupsAreNotAvailable()
        {
            // Arrange

            var mockCarPoolingService = new Mock<ICarPoolingService>();

            int groupIDMockToLoad = -1;
            mockCarPoolingService.Setup(repo => repo.RequestToBeDroppedOffService(groupIDMockToLoad)).Throws(new Exception("Test exception. Journeyis required"));
            var controller = new CarPoolingController(
               mockCarPoolingService.Object);

            int groupID = -1;
            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => controller.RequestToBeDroppedOff(groupID));
        }


        [Fact]
        public async Task AssignCarToWaitingGroup_ReturnsNotFoundResult_WhenGroupsAssignedToACar()
        {
            // Arrange

            var mockCarPoolingService = new Mock<ICarPoolingService>();

            var controller = new CarPoolingController(
               mockCarPoolingService.Object);

            int groupID = 1;

            // Act
            var result = await controller.AssignCarToWaitingGroup(groupID);


            // Assert
            Assert.IsType<NotFoundResult>(result.Result);
            var NotFoundResult = result.Result as NotFoundResult;
            Assert.Equal(404, NotFoundResult.StatusCode);
        }
    }
}


﻿using System;
using APICarPooling.Core.Interfaces;
using APICarPooling.Core.Model;
using APICarPooling.Logging.Interfaces;
using APICarPooling.Core.Infrastructure;
using APICarPooling.Core.Exceptions;

namespace APICarPooling.Persistence.Data
{
	public class CarPoolingServiceRepository : ICarPoolingService
    {

        private const string ERROR_EXCEPTION_CAR = "Cars not availables. Add new available cars.";
        private const string EXCEPTION_CAR = "There are not cars to add";
        private const string ERROR_EXCEPTION_JOURNEY = "Groups of people not registered correctly.";
        private const string EXCEPTION_JOURNEY = "There are not groups of people to perform a journey";
        private const string ERROR_EXCEPTION_DROPOFF = "The group ID is not to be found.";
        private const string EXCEPTION_DROPOFF = "Required a form with the group ID";
        private const string ERROR_EXCEPTION_LOCATE = "Car are not available because the group is not to be found.";
        private const string EXCEPTION_LOCATE = "required A url encoded form with the group ID such that ID=X";
        private const string EXCEPTION_ASSIGNED = "Required a form with the group ID to assign a car";
        private const string ERROR_EXCEPTION_ASSIGNED = "Group is not assigned to a car";
        private const string EXCEPTION_AVAILABLE_CARS = "There are not cars availables to assign to a group";

        private readonly ICarRepository _carRepository;
        private readonly ITravelerRepository _travelerRepository;
        private readonly IJourneyRepository _journeyRepository;
        private readonly ILogRepository _logRepository;

        public CarPoolingServiceRepository(ICarRepository carsRepository, ITravelerRepository travelerRepository, IJourneyRepository journeyRepository, ILogRepository logRepository)
        {
            this._carRepository = carsRepository;
            this._travelerRepository = travelerRepository;
            this._journeyRepository = journeyRepository;
            this._logRepository = logRepository;
        }
        public async Task AddAvailableCarsService(List<CarAvailable> cars)
        {
            try
            {

                if (cars.Count == 0) throw new CarException(EXCEPTION_CAR);

                await _carRepository.RemoveAllPreviousCarsAsync();
                await _carRepository.AddAvailableCarsAsync(cars);

            }
            catch (Exception ex)
            {
                await _logRepository.RegisterErrorAsync(ERROR_EXCEPTION_CAR, ex.Message);
                throw new CarException(ERROR_EXCEPTION_CAR + ex.Message);


            }
        }
        public async Task PerformJourneyService(List<Traveler> people)
        {
            try
            {
                if (people.Count == 0) throw new JourneyException(EXCEPTION_JOURNEY);

                await _travelerRepository.RequestAJourneyAsync(people);

            }
            catch (Exception ex)
            {
                await _logRepository.RegisterErrorAsync(ERROR_EXCEPTION_JOURNEY, ex.Message);
                throw new JourneyException(ERROR_EXCEPTION_JOURNEY + ex.Message);

            }

        }
        public async Task<int> RequestToBeDroppedOffService(int groupID)
        {
            try
            {
                if (groupID == 0) throw new DropOffException(EXCEPTION_DROPOFF);

                var traveler = await _travelerRepository.SelectTravelerAsync(groupID);

                if (traveler is null)
                    return 0;

                //select the journey in process
                var journey = await _journeyRepository.SelectJourneyByTravelerAsync(groupID);

                //Firstly, I will check if there a journey in process, in order to drop off the journey
                if (journey is not null)
                    await _journeyRepository.RequestToBeDroppedOffAsync(journey);

                //Secondly, I will drop off the group of people whether they traveled or not
                await _travelerRepository.RequestToBeDroppedOffAsync(traveler);

                return 1;

            }
            catch (Exception ex)
            {
                await _logRepository.RegisterErrorAsync(ERROR_EXCEPTION_DROPOFF, ex.Message);
                throw new DropOffException(ERROR_EXCEPTION_DROPOFF + ex.Message);
            }
        }
        public async Task<CarAvailable> ReturnTheCarOfTheGroupService(int groupID)
        {
            try
            {
                if (groupID == 0) throw new LocateException(EXCEPTION_LOCATE);

                var carAssigned = await _carRepository.ReturnTheCarOfTheGroupAsync(groupID);

                if (carAssigned is null)
                    return null;

                return carAssigned;

            }
            catch (Exception ex)
            {
                await _logRepository.RegisterErrorAsync(ERROR_EXCEPTION_LOCATE, ex.Message);
                throw new LocateException(ERROR_EXCEPTION_LOCATE + ex.Message);
            }
        }
        public async Task<Journey> AssignCarToWaitingGroupService(int groupID)
        {
            try
            {
                if (groupID == 0) throw new AssignException(EXCEPTION_ASSIGNED);

                var travelerGroup = await _travelerRepository.SelectTravelerAsync(groupID);

                if (travelerGroup is null)
                    return null;

                var availableCars = await _carRepository.FindAvailableCarsAsync();

                if (availableCars.Count == 0) throw new CarException(EXCEPTION_AVAILABLE_CARS);

                var carAssigned = await _journeyRepository.AssignCarToWaitingGroupToPerformAJourneyAsync(travelerGroup, availableCars);

                if (carAssigned is null)
                    return null;

                return carAssigned;
            }
            catch (Exception ex)
            {
                await _logRepository.RegisterErrorAsync(ERROR_EXCEPTION_ASSIGNED, ex.Message);
                throw new AssignException(ERROR_EXCEPTION_ASSIGNED + ex.Message);
            }
        }
    }
}


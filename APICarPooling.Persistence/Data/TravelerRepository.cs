﻿using APICarPooling.Core.Model;
using APICarPooling.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace APICarPooling.Persistence.Data
{
	public class TravelerRepository : ITravelerRepository
	{
        private readonly ApplicationDbContext _carPoolingDbcontext;

        public TravelerRepository(ApplicationDbContext carPoolingContext)
        {
            _carPoolingDbcontext = carPoolingContext;
        }

        public async Task RequestAJourneyAsync(List<Traveler> travelers)
        {
            foreach (var traveler in travelers)
            {
                if (traveler.People > 0)
                    _carPoolingDbcontext.Traveler.Add(traveler);
            }
            await _carPoolingDbcontext.SaveChangesAsync();
        }


        public async Task RequestToBeDroppedOffAsync(Traveler traveler)
        {
            _carPoolingDbcontext.Traveler.Remove(traveler);
            await _carPoolingDbcontext.SaveChangesAsync();
        }

        public async Task<Traveler> SelectTravelerAsync(int groupID)
        {
            return await _carPoolingDbcontext.Traveler.FirstOrDefaultAsync(p => p.Id == groupID);
        }
    }
}


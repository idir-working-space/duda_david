# Use the .NET SDK for building
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /car

# Copy csproj files
COPY *.sln .
COPY ./APICarPooling/*.csproj ./APICarPooling/
COPY ./APICarPooling.Core/*.csproj ./APICarPooling.Core/
COPY ./APICarPooling.Logging/*.csproj ./APICarPooling.Logging/
COPY ./APICarPooling.Persistence/*.csproj ./APICarPooling.Persistence/
COPY ./APICarPooling.Test/*.csproj ./APICarPooling.Test/

# Restore the packages of our solution
# RUN dotnet restore

# Copy everything else and build the project
COPY . .
RUN dotnet publish -c Release -o out

# Use the .NET Runtime for executing
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /car
COPY --from=build /car/out .

EXPOSE 9091
ENV ASPNETCORE_URLS=http://+:9091
ENTRYPOINT ["dotnet","APICarPooling.dll"]

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace APICarPooling.Core.Model
{
	public class Journey
	{
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("TravelerId")]
        public virtual int TravelerId { get; set; }

        [ForeignKey("CarAvailableId")]
        public virtual int CarId { get; set; }
       
    }
}


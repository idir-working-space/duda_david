﻿using System;
namespace APICarPooling.Core.Infrastructure
{
	public sealed class JourneyException : Exception
	{
		public JourneyException()
		{
		}

        public JourneyException(string message) : base(message)
        {
        }
    }
}


﻿using System;
using APICarPooling.Core.Model;

namespace APICarPooling.Core.Interfaces
{
	public interface IJourneyRepository
	{
		Task RequestToBeDroppedOffAsync(Journey journey);
        Task<Journey> SelectJourneyByTravelerAsync(int groupID);
		Task<Journey> AssignCarToWaitingGroupToPerformAJourneyAsync(Traveler groupID, List<CarAvailable> carAvailables);
    }
}

